package com.dhaba.recruitmentbukalapak.api

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dhaba.recruitmentbukalapak.data.api.ApiService
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.hamcrest.CoreMatchers
import org.junit.*
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@RunWith(JUnit4::class)
class ApiTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: ApiService

    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun tesEndpoint() {
        runBlocking {
            enqueueResponse("flash_banner.json")
            val resultResponse = service.getFlashBanner()

            val request = mockWebServer.takeRequest(1, TimeUnit.SECONDS)
            Assert.assertNotNull(resultResponse)
            Assert.assertThat(request!!.path, CoreMatchers.`is`("/flash_banners.json"))
        }
    }

    @Test
    fun getBanner(){
        runBlocking {
            enqueueResponse("flash_banner.json")
            val resultResponse = service.getFlashBanner()
            val selectedFirstBanner = resultResponse.banners[0]
            Assert.assertThat(selectedFirstBanner.description, CoreMatchers.`is`("Zona Investor Baru"))
            Assert.assertThat(selectedFirstBanner.image, CoreMatchers.`is`("https://s4.bukalapak.com/rev-banner/flash_banner/351617741/original/mobile_ZonaInvestorBaru_4b786c9b-58c0-481c-a4b8-d8b808b63cc5.jpeg"))
            Assert.assertThat(selectedFirstBanner.order, CoreMatchers.`is`(1))
            Assert.assertThat(selectedFirstBanner.info.id, CoreMatchers.`is`(0))
            Assert.assertThat(selectedFirstBanner.info.promoDue, CoreMatchers.`is`("2021-02-17T16:59:00Z"))
            Assert.assertThat(selectedFirstBanner.info.type, CoreMatchers.`is`("url"))
        }
    }

    @Test
    fun testTotalReturnData() {
        runBlocking {
            enqueueResponse("flash_banner.json")
            val resultResponse = service.getFlashBanner()
            val totalData = resultResponse.banners.size
            Assert.assertThat(totalData, CoreMatchers.`is`(10))
        }
    }

    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader!!
            .getResourceAsStream("api-response/$fileName")
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(
            source.readString(Charsets.UTF_8))
        )
    }
}