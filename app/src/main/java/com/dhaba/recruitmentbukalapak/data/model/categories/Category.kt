package com.dhaba.recruitmentbukalapak.data.model.categories


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("children")
    var children: List<Children>,
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("revamped")
    var revamped: Boolean,
    @SerializedName("url")
    var url: String
)