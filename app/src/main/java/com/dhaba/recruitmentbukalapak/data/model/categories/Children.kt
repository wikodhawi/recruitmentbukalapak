package com.dhaba.recruitmentbukalapak.data.model.categories


import com.google.gson.annotations.SerializedName

data class Children(
    @SerializedName("children")
    var children: List<ChildrenX>,
    @SerializedName("id")
    var id: Int,
    @SerializedName("name")
    var name: String,
    @SerializedName("revamped")
    var revamped: Boolean,
    @SerializedName("url")
    var url: String
)