package com.dhaba.recruitmentbukalapak.data.model.categories


import com.google.gson.annotations.SerializedName

data class CategoryProduct(
    @SerializedName("categories")
    var categories: List<Category>,
    @SerializedName("message")
    var message: Any,
    @SerializedName("status")
    var status: String
)