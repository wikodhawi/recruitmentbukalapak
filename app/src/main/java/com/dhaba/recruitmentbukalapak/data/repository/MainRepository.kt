package com.dhaba.recruitmentbukalapak.data.repository

import com.dhaba.recruitmentbukalapak.data.api.ApiHelper
import javax.inject.Inject

class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getFlashBanner() = apiHelper.getFlashBanner()

}