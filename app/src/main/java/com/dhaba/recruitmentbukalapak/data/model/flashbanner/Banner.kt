package com.dhaba.recruitmentbukalapak.data.model.flashbanner


import com.google.gson.annotations.SerializedName

data class Banner(
    @SerializedName("description")
    var description: String,
    @SerializedName("image")
    var image: String,
    @SerializedName("info")
    var info: Info,
    @SerializedName("open_new_window")
    var openNewWindow: Boolean,
    @SerializedName("order")
    var order: Int
)