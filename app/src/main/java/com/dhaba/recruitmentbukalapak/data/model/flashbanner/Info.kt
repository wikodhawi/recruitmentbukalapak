package com.dhaba.recruitmentbukalapak.data.model.flashbanner


import com.google.gson.annotations.SerializedName

data class Info(
    @SerializedName("id")
    var id: Int,
    @SerializedName("promo_due")
    var promoDue: String,
    @SerializedName("type")
    var type: String,
    @SerializedName("url")
    var url: String
)