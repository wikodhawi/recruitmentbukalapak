package com.dhaba.recruitmentbukalapak.data.api

import com.dhaba.recruitmentbukalapak.data.model.categories.CategoryProduct
import com.dhaba.recruitmentbukalapak.data.model.flashbanner.FlashBanner

interface ApiHelper {

    suspend fun getFlashBanner(): FlashBanner
    suspend fun getCategoryProduct(): CategoryProduct
}