package com.dhaba.recruitmentbukalapak.data.api

import com.dhaba.recruitmentbukalapak.data.model.categories.CategoryProduct
import com.dhaba.recruitmentbukalapak.data.model.flashbanner.FlashBanner
import retrofit2.http.GET

interface ApiService {

    @GET("flash_banners.json")
    suspend fun getFlashBanner(): FlashBanner

    @GET("categories.json")
    suspend fun getCategoryProduct(): CategoryProduct

}