package com.dhaba.recruitmentbukalapak.data.model.flashbanner


import com.google.gson.annotations.SerializedName

data class FlashBanner(
    @SerializedName("banners")
    var banners: List<Banner>,
    @SerializedName("message")
    var message: Any,
    @SerializedName("status")
    var status: String
)