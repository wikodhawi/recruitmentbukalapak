package com.dhaba.recruitmentbukalapak.data.api

import com.dhaba.recruitmentbukalapak.data.model.categories.CategoryProduct
import com.dhaba.recruitmentbukalapak.data.model.flashbanner.FlashBanner
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getFlashBanner(): FlashBanner = apiService.getFlashBanner()
    override suspend fun getCategoryProduct(): CategoryProduct = apiService.getCategoryProduct()
}