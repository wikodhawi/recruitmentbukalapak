package com.dhaba.recruitmentbukalapak.viewmodel.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dhaba.recruitmentbukalapak.data.api.ApiResult
import com.dhaba.recruitmentbukalapak.data.model.flashbanner.FlashBanner
import com.dhaba.recruitmentbukalapak.data.repository.MainRepository
import kotlinx.coroutines.launch

class MainViewModel @ViewModelInject constructor(
    private val repository: MainRepository
) : ViewModel() {
    private val _flashBanner = MutableLiveData<ApiResult<FlashBanner>>()
    val flashBanner : LiveData<ApiResult<FlashBanner>> get() = _flashBanner

    fun getFlashBanner() {
        viewModelScope.launch {
            _flashBanner.postValue(ApiResult.loading())
            try {
                val contacts =repository.getFlashBanner()
                _flashBanner.postValue(ApiResult.success(contacts))
            }
            catch (e: Exception) {
                _flashBanner.postValue(ApiResult.error(e.localizedMessage))
            }
        }
    }
}