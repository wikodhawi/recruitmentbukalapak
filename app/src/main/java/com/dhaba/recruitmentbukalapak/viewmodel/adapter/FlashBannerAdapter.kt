package com.dhaba.recruitmentbukalapak.viewmodel.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.dhaba.recruitmentbukalapak.data.model.flashbanner.Banner
import com.dhaba.recruitmentbukalapak.databinding.ItemBannerBinding
import com.smarteist.autoimageslider.SliderViewAdapter

class FlashBannerAdapter(private val listBanner: List<Banner>) : SliderViewAdapter<FlashBannerAdapter.SliderAdapter>() {
    override fun getCount(): Int {
        return listBanner.size
    }

    override fun onBindViewHolder(viewHolder: SliderAdapter?, position: Int) {
        Glide.with(viewHolder?.binding?.root?.context!!).load(listBanner[position].image)
            .into(viewHolder.binding.imgBanner)
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderAdapter {
        val binding = ItemBannerBinding.inflate(LayoutInflater.from(parent?.context), parent, false)
        return SliderAdapter(binding)
    }

    class SliderAdapter(val binding: ItemBannerBinding) : SliderViewAdapter.ViewHolder(binding.root)
}