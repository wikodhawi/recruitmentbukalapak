package com.dhaba.recruitmentbukalapak.view.main

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import com.dhaba.recruitmentbukalapak.R
import com.dhaba.recruitmentbukalapak.base.BaseActivity
import com.dhaba.recruitmentbukalapak.data.api.ApiResult
import com.dhaba.recruitmentbukalapak.databinding.ActivityMainBinding
import com.dhaba.recruitmentbukalapak.utilities.CustomDialog
import com.dhaba.recruitmentbukalapak.viewmodel.adapter.FlashBannerAdapter
import com.dhaba.recruitmentbukalapak.viewmodel.main.MainViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainViewModel by viewModels()
    private lateinit var dialog: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        observeViewModel()
        viewModel.getFlashBanner()
        setupToolbar()
        binding.swpMain.setOnRefreshListener {
            viewModel.getFlashBanner()
            binding.swpMain.isRefreshing = false
        }
    }

    private fun observeViewModel() {
        viewModel.flashBanner.observe(this, {
            when (it.status) {
                ApiResult.Status.LOADING -> {
                    binding.sldJobVacancyHome.visibility = View.GONE
                    binding.lytLoadingUser.root.visibility = View.VISIBLE
                }
                ApiResult.Status.ERROR -> {
                    binding.sldJobVacancyHome.visibility = View.GONE
                    binding.lytLoadingUser.root.visibility = View.VISIBLE
                    if(!this::dialog.isInitialized || !dialog.isShowing) {
                        dialog = CustomDialog.createDialogWithTwoButton(this,
                            cancelable = false,
                            message = it.message?: "",
                            labelPositiveButton = getString(R.string.retry),
                            labelNegativeButton = getString(R.string.settings),
                            positiveAction = {
                                viewModel.getFlashBanner()
                            },
                            negativeAction = {
                                startActivityForResult(Intent(Settings.ACTION_SETTINGS), 0)
                            },
                            closeAction = {
                            })
                        dialog.show()
                    }
                    binding.lytLoadingUser.root.visibility = View.VISIBLE
                }
                ApiResult.Status.SUCCESS -> {
                    binding.sldJobVacancyHome.sliderAdapter = FlashBannerAdapter(it.data?.banners!!)
                    binding.sldJobVacancyHome.visibility = View.VISIBLE
                    binding.lytLoadingUser.root.visibility = View.GONE
                }
            }
        })
    }

    private fun setupToolbar()
    {
        binding.toolbar.lblTitleToolbarHome.text = getString(R.string.flash_banner)
    }

}