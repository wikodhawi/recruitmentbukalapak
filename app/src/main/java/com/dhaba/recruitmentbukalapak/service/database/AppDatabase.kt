package com.dhaba.recruitmentbukalapak.service.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dhaba.recruitmentbukalapak.data.model.entity.DummyEntity
import com.dhaba.recruitmentbukalapak.service.database.dao.DummyEntityDao


@Database(
    entities =[DummyEntity::class], version = 1
)

@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun dummyEntityDao(): DummyEntityDao
}
