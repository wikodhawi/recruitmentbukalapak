package com.dhaba.recruitmentbukalapak.utilities

enum class SharedPrefKeys {
    SHARED_NAME,
    TES_SHARED,
    TOKEN,
    LANGUAGE,
    USER_ID,
    USER_NAME,
    NAME,
    IMAGE_PROFILE,
    IS_LOGIN,
    FIREBASE_TOKEN,
    LATITUDE,
    LONGITUDE,
    PETANI_ID,
    VERSION_CODE,
    DOWNLOAD_LINK,
    UPGRADE_MANDATORY,
    FONT_SIZE_PREF,
    PIN,
    IS_DARK_MODE
}